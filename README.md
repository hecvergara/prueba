**Instrucciones para ejecutar el proyecto**

El proyecto se puede ejecutar en 2 navegadores, Chrome y Firefox.

Para ejecutar el proyecto en el navegador Chrome se debe ejecutar el siguiente comando:

gradle clean test -Pwebdriver.driver=chrome

Para ejecutar el proyecto en el navegador Firefox se debe ejecutar el siguiente comando:

gradle clean test -Pwebdriver.driver=firefox