### Como ejecutar la prueba:
 
El proyecto se puede ejecutar en 2 navegadores, Firefox o Chrome.

Si se desea ejecutar en Chrome, se deje ejeuctar el siguiente comando:

gradle clean test -Pwebdriver.driver=chrome

Si se desea ejecutar en Firefox se debe ejecutar el siguiente comando:

gradle clean test -Pwebdriver.driver=firefox

