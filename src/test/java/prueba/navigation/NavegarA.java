package prueba.navigation;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

public class NavegarA {
    /****
     *
     * Este metodo lo que hace es levantar el navegador con la pagina que ya se tiene configurada en IrPaginaPrincipal
     */
    public static Performable paginaPrincipalCompras() {
        return Task.where("{0} Se encuentra en la pagina principal",
                Open.browserOn().the(IrPaginaPrincipal.class)
        );
    }
}