package prueba.userinterface;

import org.openqa.selenium.By;

public class Login {
    public static By USERNAME_TXT = By.id("user-name");
    public static By PASSWORD_TXT = By.id("password");
    public static By LOGIN_BTN = By.className("btn_action");
}
