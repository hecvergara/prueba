package prueba.userinterface;

import org.openqa.selenium.By;

/**
 * En esta clase se mapena los elementos que componen el proceso de compra
 */
public class Comprar {
    public static By ADDTOCART_BTN = By.xpath("//div[@class='inventory_list']//div[1]//div[3]//button[1]");
    public static By VERIFICARCARRITO_LBL = By.xpath("//span[@class='fa-layers-counter shopping_cart_badge']");
    public static By CARRITO_IMG = By.xpath("//*[name()='path' and contains(@fill,'currentCol')]");
    public static By CHECKOUT_BTN = By.xpath("//a[@class='btn_action checkout_button']");
    public static By FIRSTNAME_TXT = By.id("first-name");
    public static By LASTNAME_TXT = By.id("last-name");
    public static By POSTALCODE_TXT = By.id("postal-code");
    public static By CONTINUE_BTN = By.xpath("//input[@class='btn_primary cart_button']");
    public static By FINISH_BTN = By.xpath("//a[@class='btn_action cart_button']");
    public static By MENSAJECOMPRA_LBL = By.xpath("//h2[@class='complete-header']");

}
